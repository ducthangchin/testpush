package com.efimchick.tasks.segments;

public class Segment {
    Point start ;
    Point end ;
    public Segment(Point start, Point end) {
        if (Math.abs(start.getX()- end.getX())+Math.abs(start.getY()-end.getY())==0) {
            System.out.println("The start point and the end point must be different!");
        }
        else {
            this.start = start;
            this.end = end;
        }
    }

    double length() {
        return Math.sqrt(Math.pow((start.getX()- end.getX()),2)+Math.pow((start.getY()-end.getY()),2));
    }

    Point middle() {
        double tmpx = 0.5*(start.getX()+end.getX());
        double tmpy = 0.5*(start.getY()+end.getY());
        Point midpoint = new Point(tmpx,tmpy);
        return midpoint;
    }
//
    Point intersection(Segment another) {
        double x1 = start.getX(); double x2 = end.getX();
        double y1 = start.getY(); double y2 = end.getY();
        double X1 = another.start.getX(); double X2 = another.end.getX();
        double Y1 = another.start.getY(); double Y2 = another.end.getY();
        /*
        a1.x + b1.y = c1
        a2.x + b2.y = c2
         */

        double a1, b1, c1;
        a1 = -(y1-y2); b1 = (x1-x2); c1 = -x1*(y1-y2)+y1*(x1-x2);
        double a2, b2, c2;
        a2 = -(Y1-Y2); b2 = (X1-X2); c2 = -X1*(Y1-Y2)+Y1*(X1-X2);
        double D, Dx, Dy;
        D = a1*b2 - a2*b1;
        if (D == 0)
            return null;
        Dx = c1*b2 - c2*b1;
        Dy = a1*c2 - a2*c1;
        Point tmp = new Point(Dx/D,Dy/D);
        return tmp;
    }

}
